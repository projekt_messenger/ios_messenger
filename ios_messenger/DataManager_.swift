//
//  DataManager_.swift
//  ios_messenger
//
//  Created by Oldřich Hejčl on 08/01/2020.
//  Copyright © 2020 Oldřich Hejčl. All rights reserved.
//
struct People {
  let name: String
    let surname: String
}

extension People: Decodable {

  private enum Key: String, CodingKey {
    case name = "name"
    case surname = "surname"
  }

  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: Key.self)

    self.name = try container.decode(String.self, forKey: .name)
    self.surname = try container.decode(String.self, forKey: .surname)
  }
}

import Foundation
import Foundation
class DataManager_ {
    
    
    
    var allUsers: [User] = []
    
    init() {
        
        getUsers()
    }
    
    /*func getUsers() {
        self.allUsers = [User]()
        self.allUsers.append(User(_name: "Adam", _surname: "Novák", _username: "rrthr", _email: "novka@gmail.com", _password: "abcd", _gender: "male", _activity: "offline", _lastMessage: "No ahoj"))
        self.allUsers.append(User(_name: "Adriana", _surname: "Maskalová", _username: "maska", _email: "novka@gmail.com", _password: "abcd", _gender: "female", _activity: "offline", _lastMessage: "Čau"))
        self.allUsers.append(User(_name: "Džanghí", _surname: "Park", _username: "thrh", _email: "novka@gmail.com", _password: "abcd", _gender: "male", _activity: "online", _lastMessage: "Chceš se prát?"))
        self.allUsers.append(User(_name: "Aneta", _surname: "Vlasatá", _username: "tjzjtzj", _email: "novka@gmail.com", _password: "abcd", _gender: "female", _activity: "offline", _lastMessage: "Nazdar"))
        self.allUsers.append(User(_name: "Lukáš", _surname: "Srbák", _username: "dfg", _email: "novka@gmail.com", _password: "abcd", _gender: "male", _activity: "online", _lastMessage: ":D"))
        self.allUsers.append(User(_name: "Steven", _surname: "Buchanan", _username: "gfghfgh", _email: "novka@gmail.com", _password: "abcd", _gender: "male", _activity: "online", _lastMessage: "sdfsdfsdf"))
        self.allUsers.append(User(_name: "Nancy", _surname: "Davolio", _username: "gdfdg", _email: "novka@gmail.com", _password: "abcd", _gender: "female", _activity: "offline", _lastMessage: "Jdeme domu a cestou koupíme něco k večeři"))
        self.allUsers.append(User(_name: "Andrew", _surname: "Fuller", _username: "sdf", _email: "novka@gmail.com", _password: "abcd", _gender: "male", _activity: "online", _lastMessage: "cus"))
        self.allUsers.append(User(_name: "Andrea", _surname: "Zmatená", _username: "jdks", _email: "novka@gmail.com", _password: "abcd", _gender: "female", _activity: "online", _lastMessage: "ok"))
        self.allUsers.append(User(_name: "Andrea", _surname: "Ztracená", _username: "flfk", _email: "novka@gmail.com", _password: "abcd", _gender: "female", _activity: "offline", _lastMessage: "jasne"))
    }*/
    func getUsers() {
        self.allUsers = [User]()
        
        let defaults = UserDefaults.standard
        //let token = defaults.object(forKey: "token")
        if let token = defaults.object(forKey: "token") as? String {
            //print(token)
            let url = URL(string:"http://10.0.0.32:8080/api/User")! //"http://90.178.65.28:8080/api/auth/login")!
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")

            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    
                    // Check for Error
                    if let error = error {
                        print("Error took place \(error)")
                        return
                    }
             
                    // Convert HTTP Response Data to a String
                    if let data = data, let dataString = String(data: data, encoding: .utf8) {
                        
                          //print("Response data string:\n \(dataString)")
                        self.jsonToString(data: data)
                        
                        

                        do {
                          let people = try JSONDecoder().decode([People].self, from: data)
                            for p in people {
                                print(p.name)
                                self.allUsers.append(User(_name: p.name, _surname: p.surname, _email: "novka@gmail.com", _password: "abcd", _gender: "male", _lastMessage: "No ahoj"))
                            }
                        } catch {
                          // I find it handy to keep track of why the decoding has failed. E.g.:
                          print(error)
                          // Insert error handling here
                        }
                        
                        
                        
                    }
                //print(response)
            }
            task.resume()
        }
        
        
        
        
    }
    
    func jsonToString(data: Data){
        do {
                //if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                //if let token = json["name"] as? String {
                    //print(token)
            //}
                    //print(data)
                    
                //}
            }
            
        catch let err {
            print(err.localizedDescription)
        }

    }
}
