//
//  ContactsTableViewController.swift
//  ios_messenger
//
//  Created by Oldřich Hejčl on 01/01/2020.
//  Copyright © 2020 Oldřich Hejčl. All rights reserved.
//

import UIKit
import ProgressHUD

class ContactsTableViewController: UITableViewController, UISearchResultsUpdating {
    
    
    
    @IBOutlet weak var headerView: UIView!
    
    var allUsers: [User] = []
    var filteredUsers: [User] = []
    var allUsersGroupped = NSDictionary() as! [String : [User]]
    var dtMng = DataManager_()
    
    let searchController = UISearchController(searchResultsController: nil)
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        LoadUsers()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return allUsers.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        as! ContactsTableViewCell
        
        cell.generateUserWith(User: allUsers[indexPath.row], IndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    
    func LoadUsers() {
        
        ProgressHUD.show()
        
        self.allUsers = []
        self.filteredUsers = []
        self.allUsersGroupped = [:]
        
        self.allUsers = dtMng.allUsers
        
        /*self.allUsers = [User]()
        self.allUsers.append(User(_name: "Adam", _surname: "Novák", _username: "navka", _email: "novka@gmail.com", _password: "abcd", _gender: "male"))
        self.allUsers.append(User(_name: "Adriana", _surname: "Maskalová", _username: "maskalka", _email: "novka@gmail.com", _password: "abcd", _gender: "female"))
        self.allUsers.append(User(_name: "Džanghí", _surname: "Park", _username: "navka", _email: "novka@gmail.com", _password: "abcd", _gender: "male"))
        self.allUsers.append(User(_name: "Aneta", _surname: "Vlasatá", _username: "navka", _email: "novka@gmail.com", _password: "abcd", _gender: "female"))
        self.allUsers.append(User(_name: "Lukáš", _surname: "Srbák", _username: "navka", _email: "novka@gmail.com", _password: "abcd", _gender: "male"))
        self.allUsers.append(User(_name: "Steven", _surname: "Buchanan", _username: "navka", _email: "novka@gmail.com", _password: "abcd", _gender: "male"))
        self.allUsers.append(User(_name: "Nancy", _surname: "Davolio", _username: "navka", _email: "novka@gmail.com", _password: "abcd", _gender: "female"))
        self.allUsers.append(User(_name: "Andrew", _surname: "Fuller", _username: "navka", _email: "novka@gmail.com", _password: "abcd", _gender: "male"))
        self.allUsers.append(User(_name: "Andrea", _surname: "Zmatená", _username: "navka", _email: "novka@gmail.com", _password: "abcd", _gender: "female")) */
        
        
        self.tableView.reloadData()
        ProgressHUD.dismiss()
        
    }
    
    func filterContentForSearch(searchText:String, scope:String = "All"){
        
        filteredUsers = allUsers.filter({ (User) -> Bool in
            return User.name.lowercased().contains(searchText.lowercased())
        })
        
        tableView.reloadData()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
        filterContentForSearch(searchText: searchController.searchBar.text!)
        
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
