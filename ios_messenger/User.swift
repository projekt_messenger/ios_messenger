//
//  User.swift
//  ios_messenger
//
//  Created by Oldřich Hejčl on 19/12/2019.
//  Copyright © 2019 Oldřich Hejčl. All rights reserved.
//

import Foundation

class User {

    var id:UUID
    var name:String
    var surname:String
    var username:String
    var email:String
    var gender:String
    var activity:String
    var lastMessage:String
    
    
    init(_name:String, _surname:String, _username:String, _email:String,
         _password:String, _gender:String, _activity:String, _lastMessage:String) {
        
        id = UUID()
        name = _name
        surname = _surname
        username = _username
        email = _email
        gender = _gender
        activity = _activity
        lastMessage = _lastMessage
        
    }
    
    
}
