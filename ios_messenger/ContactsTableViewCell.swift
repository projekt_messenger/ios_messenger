//
//  ContactsTableViewCell.swift
//  ios_messenger
//
//  Created by Oldřich Hejčl on 19/12/2019.
//  Copyright © 2019 Oldřich Hejčl. All rights reserved.
//

import UIKit

class ContactsTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var userActivityImageView: UIImageView!
    
    @IBOutlet weak var userNameView: UILabel!
    
    var indexPath:IndexPath!
    
    let tapGestureRecognizer = UITapGestureRecognizer()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tapGestureRecognizer.addTarget(self, action: #selector(self.userTap))
        userImageView.isUserInteractionEnabled = true
        userImageView.addGestureRecognizer(tapGestureRecognizer)
       
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
       
    }
    
    func generateUserWith(User: User, IndexPath: IndexPath) {
        
        self.userNameView.text = User.name + " " + User.surname
        self.indexPath = IndexPath
        //self.userImageView.image = UIImage(named: "male_avatar.png")
        
        if (User.gender == "male") {
            self.userImageView.image = UIImage(named: "male_avatar.png")
        }
        else {
            self.userImageView.image = UIImage(named: "female_avatar.png")
        }
        if (User.activity == "online") {
            self.userActivityImageView.image = UIImage(named: "online_png.png")
        }
        else {
            self.userActivityImageView.image = UIImage(named: "offline_png.png")
        }
        
        
    }
    
    @objc func userTap() {
        print("user tap at index \(indexPath)")
    }
    

}
