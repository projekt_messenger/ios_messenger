//
//  ChatsViewController.swift
//  ios_messenger
//
//  Created by Oldřich Hejčl on 05/01/2020.
//  Copyright © 2020 Oldřich Hejčl. All rights reserved.
//

import UIKit

class ChatsViewController: UIViewController,
UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    var allUsers: [User] = []
    var dtMng = DataManager_()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaults = UserDefaults.standard
        let token = defaults.object(forKey: "token")
        
        //print("\(token) chats view")

        loadUsers()
        
        self.dtMng.getUsers()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    @IBAction func createNewChat(_ sender: Any) {
        
        let userVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "usersTableView") as! ContactsTableViewController
        
        self.navigationController?.pushViewController(userVC, animated: true)
    }
    
    @IBAction func createNewPhoto(_ sender: Any) {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return allUsers.count
       }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
           let cell = tableView.dequeueReusableCell(withIdentifier: "CellChats", for: indexPath)
           as! ChatsTableViewCell
           
           cell.generateUserWith(User: allUsers[indexPath.row], IndexPath: indexPath)
           
           return cell
       }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        dismissKeyboard()
        
        let chatVC = ChatViewController()
        
        chatVC.hidesBottomBarWhenPushed = true
        
        navigationController?.pushViewController(chatVC, animated: true)
    }
    
       
    func loadUsers() {
        self.allUsers = []
        self.allUsers = dtMng.allUsers
        
    }
    func dismissKeyboard(){
        self.view.endEditing(false)
    }
       
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
