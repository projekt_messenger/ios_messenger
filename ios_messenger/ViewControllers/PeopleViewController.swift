//
//  PeopleViewController.swift
//  ios_messenger
//
//  Created by Oldřich Hejčl on 05/01/2020.
//  Copyright © 2020 Oldřich Hejčl. All rights reserved.
//

import UIKit

class PeopleViewController: UIViewController,
UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var allUsers: [User] = []
    var dtMng = DataManager_()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "usersTableView") as! ContactsTableViewController
        loadUsers()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellPeople", for: indexPath)
        as! PeopleTableViewCell
        
        cell.generateUserWith(User: allUsers[indexPath.row], IndexPath: indexPath)
        
        return cell
    }
    
    func loadUsers() {
        self.allUsers = []
        self.allUsers = dtMng.allUsers
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
