//
//  WelcomeViewController.swift
//  ios_messenger
//
//  Created by Oldřich Hejčl on 18/12/2019.
//  Copyright © 2019 Oldřich Hejčl. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    
    
    
    @IBOutlet weak var emailTextField: UITextField!
    

    
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    @IBAction func tapGesture(_ sender: Any) {
        dismissKeyboard()
    }
    @IBAction func loginButton(_ sender: Any) {
        //dismissKeyboard()
        //performSegue(withIdentifier: "WelcomeToContacts", sender: self)
        let email: String = emailTextField.text!
        let password: String = passwordTextField.text!
        
        let str = "{\"Email\":\"" + email + "\"," + "\"Password\":\"" + password + "\"}"
        
        let jsonData2 = str.data(using: String.Encoding.utf8);
        //let jsonData: Data? = str.data(using: .utf8)
        
        //print(jsonData2)

        // create post request
        let url = URL(string:"http://10.0.0.32:8080/api/auth/login")! //"http://90.178.65.28:8080/api/auth/login")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        // insert json data to the request
        request.httpBody = jsonData2
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                // Check for Error
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
         
                // Convert HTTP Response Data to a String
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    //print(data)
                    
                    //print("Response data string:\n \(dataString)")
                    //let token = data["token"] as? String
                    //print(token)
                    //print(self.getStringFromJSON(json: data, key: "token", defaultValue: "nic"))
                    //let d = dataString.
                    self.jsonToString(data: data)
                }
                
        
            
            
        }
        task.resume()
        
        
        self.goToApp()
    }
    
    
    
    
    @IBAction func registerButton(_ sender: Any) {
        dismissKeyboard()
        
        
        performSegue(withIdentifier: "WelcomeToRegister", sender: self)
    }
    
    
    
    func dismissKeyboard(){
        self.view.endEditing(false)
    }    
    func cleanTextFields(){
        emailTextField.text = " "
        passwordTextField.text = " "
    }
    
    
    //MARK: Navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "WelcomeToRegister"{
            let vc = segue.destination as! RegisterViewController
            
        }
        if segue.identifier == "WelcomeToContacts"{
            //let vc = segue.destination as! ContactsViewController
                   
        }
    }
    
    func goToApp(){
        
        cleanTextFields()
        dismissKeyboard()
        
        let mainView = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "mainApplication") as! UITabBarController
        
        self.present(mainView, animated: false, completion: nil)
    }
    
    func jsonToString(data: Data){
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
            if let token = json["token"] as? String {
                //print(token)
                let defaults = UserDefaults.standard
                defaults.setValue( token, forKey: "token")
                defaults.synchronize()
                }
            }
        }
        catch let err {
            print(err.localizedDescription)
        }

    }
    
    func getStringFromJSON(json: Any, key: String, defaultValue: String) -> String {
        if let j = json as? [String : AnyObject] {
            if let returnString = j[key] as? String {
                return returnString
            }
        }

        return defaultValue
    }
    
    
    
}
