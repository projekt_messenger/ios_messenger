//
//  RegisterViewController.swift
//  ios_messenger
//
//  Created by Oldřich Hejčl on 18/12/2019.
//  Copyright © 2019 Oldřich Hejčl. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    @IBAction func tapGesture(_ sender: Any) {
        dismissKeyboard()
    }
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var surnameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func registerButton(_ sender: Any) {
        let email: String = emailTextField.text!
        let password: String = passwordTextField.text!
        let name: String = nameTextField.text!
        let surname: String = surnameTextField.text!
        
        let str = "{\"Email\":\"" + email + "\"," + "\"Password\":\"" + password + "\"," + "\"Name\":\"" + name + "\"," + "\"Surname\":\"" + surname + "\"}"
        
        let json = ["Email": email,
                    "Password": password,
                    "Name":name,
                    "Surname":surname
                    ]
        
        print(json)
        
        let jsonData2 = str.data(using: String.Encoding.utf8);
        //let jsonData: Data? = str.data(using: .utf8)
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        print(jsonData2)

        // create post request
        let url = URL(string: "http://90.178.65.28:8080/api/auth/register")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        // insert json data to the request
        request.httpBody = jsonData2
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                // Check for Error
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
         
                // Convert HTTP Response Data to a String
            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    print("Response data string:\n \(dataString)")
                }
        }
        task.resume()
        /*
        NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main) {(response, data, error) in
            guard let data = data else { return }
            print(String(data: data, encoding: .utf8)!)
        }*/
        /*let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                print(responseJSON)
            }
        }*/

        //task.resume()
        
    }
    func dismissKeyboard(){
        self.view.endEditing(false)
    }
    func cleanTextFields(){
        emailTextField.text = " "
        passwordTextField.text = " "
    }
    
    @IBAction func backButton(_ sender: Any) {
    }
    
}
